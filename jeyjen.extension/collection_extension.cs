﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jeyjen.extension
{
    public static class collection_extension
    {
        public static string join_to_string<T>(this T[] array, string separator, bool add_to_end = false)
        {
            var res = new StringBuilder();
            if (array.Length > 0)
            {
                res.Append(array[0]);
                for (int i = 1; i < array.Length; i++)
                {
                    res.AppendFormat("{0}{1}", separator, array[i].ToString());
                }
                if (add_to_end)
                {
                    res.Append(separator);
                }
            }
            return res.ToString();
        }

        public static string join_to_string<T>(this IEnumerable<T> collection, string separator, bool add_to_end = false)
        {
            var res = new StringBuilder();
            var e = collection.GetEnumerator();
            if (e.MoveNext())
            {
                res.Append(e.Current.value_is_null(""));
            }
            while (e.MoveNext())
            {
                res.AppendFormat("{0}{1}", separator, e.Current.value_is_null(""));
            }
            if (add_to_end)
            {
                res.Append(separator);
            }
            return res.ToString();
        }

        public static bool IsEmpty<T>(this T[] array)
        {
            return array == null || array.Length == 0;
        }

        // dic
        public static TV GetValueOrDefault<TK, TV>(this IDictionary<TK, TV> dict, TK key, TV defaultValue)
        {
            return dict.ContainsKey(key) ? dict[key] : defaultValue;
        }

        // сравнение
        /// <summary>
        /// Проверяет равенство переменной одному из заданных значений, используя компаратор по умолчанию для типа Т
        /// </summary>
        /// <typeparam name="T">Тип сравниваемых элементов</typeparam>
        /// <param name="variable">Переменная</param>
        /// <param name="constants">Список проверяемых значений</param>
        /// <returns>True, если переменная равна хотя бы одному из значений. False - если совпадений не найдено</returns>
        public static bool OneOf<T>(this T variable, params T[] constants)
        {
            return OneOf(variable, EqualityComparer<T>.Default, constants);
        }
        /// <summary>
        /// Проверяет равенство переменной одному из заданных значений, используя заданный компаратор
        /// </summary>
        /// <typeparam name="T">Тип сравниваемых элементов</typeparam>
        /// <param name="variable">Переменная</param>
        /// <param name="constants">Список проверяемых значений</param>
        /// <param name="comparer">Компаратор</param>
        /// <returns>True, если переменная равна хотя бы одному из значений. False - если совпадений не найдено</returns>
        public static bool OneOf<T>(this T variable, IEqualityComparer<T> comparer, params T[] constants)
        {
            return constants.Any(c => comparer.Equals(c, variable));
        }

        /// <summary>
        /// Проверяет неравенство переменной ни одному из заданных значений, используя компаратор по умолчанию для типа Т
        /// </summary>
        /// <typeparam name="T">Тип сравниваемых элементов</typeparam>
        /// <param name="variable">Переменная</param>
        /// <param name="constants">Список проверяемых значений</param>
        /// <returns>True, если переменная не равна ни одному из значений. False - если есть совпадения</returns>
        public static bool NotIn<T>(this T variable, params T[] constants)
        {
            return NotIn(variable, EqualityComparer<T>.Default, constants);
        }
        /// <summary>
        /// Проверяет неравенство переменной ни одному из заданных значений, используя заданный компаратор
        /// </summary>
        /// <typeparam name="T">Тип сравниваемых элементов</typeparam>
        /// <param name="variable">Переменная</param>
        /// <param name="constants">Список проверяемых значений</param>
        /// <param name="comparer">Компаратор</param>
        /// <returns>True, если переменная не равна ни одному из значений. False - если есть совпадения</returns>
        public static bool NotIn<T>(this T variable, IEqualityComparer<T> comparer, params T[] constants)
        {
            return !constants.Any(c => comparer.Equals(c, variable));
        }

        /// <summary>
        /// (перенесено из старого кода)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="variable"></param>
        /// <param name="constants"></param>
        /// <returns></returns>
        public static bool OneOf<T>(this T variable, IEnumerable<T> constants)
        {
            return constants.Contains(variable);
        }

        /// <summary>
        /// (перенесено из старого кода)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="variable"></param>
        /// <param name="constants"></param>
        /// <returns></returns>
        public static bool NotIn<T>(this T variable, IEnumerable<T> constants)
        {
            return !constants.Contains(variable);
        }

        /// <summary>
        /// Проверяет неравенство переменной значению по умолчанию для ее типа
        /// </summary>
        /// <typeparam name="T">Тип переменной</typeparam>
        /// <param name="variable">Переменная</param>
        /// <returns>True - если значение переменной отличается от значения по умолчанию для ее типа, в противном случае - False</returns>
        public static bool NotDefault<T>(this T variable)
        {
            return !Equals(variable, default(T));
        }

        /// <summary>
        /// Метод симметричный LINQ Contains - принимает на вход значения для сравнения.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="variable"></param>
        /// <param name="constants"></param>
        /// <returns></returns>
        public static bool In<T>(this T variable, params T[] constants)
        {
            return constants.Contains(variable);
        }


        // xml
        /// <summary>
        /// Преобразует строку с XML в словарь. XML должен состоять из корневого узла и набора дочерних с уникальными именами
        /// </summary>
        /// <param name="nodeWithChildElements">Строка XML</param>
        /// <param name="trimValues">Удалять пробелы с обоих сторон значения</param>
        /// <returns>Словарь содержащий значения дочерних элементов</returns>
        public static IDictionary<string, object> ToDictionary(this string nodeWithChildElements, bool trimValues = false)
        {
            //var xElement = XElement.Parse(nodeWithChildElements);
            //return xElement.Descendants().ToDictionary(x => x.Name.LocalName, x => trimValues ? x.Value.Trim() : x.Value as object);
            return null;
        }

    }
}
