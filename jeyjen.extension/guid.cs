﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace jeyjen.extension
{
    public enum guid_type
    {
        /// <summary>
        /// по умолчанию
        /// </summary>
        none,
        /// <summary>
        /// формируется как строка
        /// </summary>
        as_string,

        /// <summary>
        /// формируется массив байтов 
        /// </summary>
        as_binary,

        /// <summary>
        /// случайная часть находится в начале
        /// </summary>
        at_end,
    }

    /// <summary> http://www.interface.ru/home.asp?artId=29255
    /// Database  	            GUID Column 	    SequentialGuidType Value 
    /// Microsoft SQL Server    uniqueidentifier    at_end
    /// MySQL                   char(36)            as_string
    /// Oracle                  raw(16)             as_binary
    /// PostgreSQL              uuid                as_string
    /// SQLite                  varies              varies
    /// </summary>
    public static class guid
    {
        private static readonly RNGCryptoServiceProvider Rng = new RNGCryptoServiceProvider();
        public static Guid generate(guid_type type = guid_type.none)
        {
            var random_bytes = new byte[10];
            Rng.GetBytes(random_bytes);
            var ts = DateTime.Now.Ticks / 10000L;
            var timestamp_bytes = BitConverter.GetBytes(ts);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(timestamp_bytes);
            }

            var guid_bytes = new byte[16];

            switch (type)
            {
                case guid_type.none:
                    {
                        return Guid.NewGuid();
                    }
                    break;
                case guid_type.as_binary:
                case guid_type.as_string:
                    {
                        Buffer.BlockCopy(timestamp_bytes, 2, guid_bytes, 0, 6);
                        Buffer.BlockCopy(random_bytes, 0, guid_bytes, 6, 10);
                        // If formatting as a string, we have to reverse the order 
                        // of the Data1 and Data2 blocks on little-endian systems. 
                        if (type == guid_type.as_string && BitConverter.IsLittleEndian)
                        {
                            Array.Reverse(guid_bytes, 0, 4);
                            Array.Reverse(guid_bytes, 4, 2);
                        }
                    }
                    break;
                case guid_type.at_end:
                    {
                        Buffer.BlockCopy(random_bytes, 0, guid_bytes, 0, 10);
                        Buffer.BlockCopy(timestamp_bytes, 2, guid_bytes, 10, 6);
                    }
                    break;
            }
            return new Guid(guid_bytes);
        }
    }
}
