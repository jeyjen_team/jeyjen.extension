﻿using Antlr3.ST;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace jeyjen.extension
{
    public static class jobject_extension
    {
        public static T value<T>(this JObject s, string key)
        {
            if (typeof(T).IsPrimitive)
            {
                return s.Value<T>(key);
            }
            else if (typeof(string).is_equals(typeof(T)))
            {
                return s.Value<T>(key);
            }
            return s.GetValue(key).ToObject<T>();
            // https://www.newtonsoft.com/json/help/html/SerializingJSONFragments.htm
        }

        public static List<T> list<T>(string key)
        {
            return null;
        } 

    }
}
