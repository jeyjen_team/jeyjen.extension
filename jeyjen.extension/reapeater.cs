﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace jeyjen.extension
{
    public class repeater
    {
        bool _alive;
        Func<Task> _action;
        int _ms;
        public repeater(Func<Task> action, int ms)
        {
            _alive = false;
            _action = action;
            _ms = ms;
        }
        public void start()
        {
            if (!_alive)
            {
                _alive = true;
                Task.Run(async () =>
                {
                    while (_alive)
                    {
                        await _action();
                        Thread.Sleep(_ms);
                    }
                });

            }
        }
        public void stop()
        {
            _alive = false;
        }
    }
}
