﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Text;

namespace jeyjen.extension
{
    public class flex : DynamicObject, IDynamicMetaObjectProvider
    {
        private Dictionary<string, dynamic> _members;

        public flex(string json = null)
        {
            _members = new Dictionary<string, dynamic>();
            if (json != null)
            {
                try
                {
                    parse_root(this, JObject.Parse(json));
                }
                catch (Exception e)
                {
                    throw new ArgumentException("error parse json: \"{0}\"".format(json), e);
                }
            }
        }
        public flex(JObject json)
        {
            _members = new Dictionary<string, dynamic>();
            if (json != null)
            {
                try
                {
                    parse_root(this, json);
                }
                catch (Exception e)
                {
                    throw new ArgumentException("error parse json: \"{0}\"".format(json), e);
                }
            }
        }
        #region json parse
        private static void parse_root(flex ent, JObject json)
        {
            foreach (var item in json.Properties())
            {
                ent[item.Name] = parse_member(item.Value);
            }
        }
        private static flex parse_object(JObject obj)
        {
            var o = new flex();
            foreach (var item in obj.Properties())
            {
                o[item.Name] = parse_member(item.Value);
            }
            return o;
        }
        private static dynamic parse_member(JToken value)
        {
            dynamic result = null;
            switch (value.Type)
            {
                case JTokenType.Array:
                    {
                        var arr = (JArray)value;
                        result = new List<dynamic>(arr.Count);
                        foreach (var it in arr)
                        {
                            result.Add(parse_member(it));
                        }
                    }
                    break;
                case JTokenType.Object:
                    {
                        result = parse_object((JObject)value);
                    }
                    break;
                case JTokenType.Boolean:
                    {
                        result = (bool)value;
                    }
                    break;
                case JTokenType.Integer:
                    {
                        result = (long)value;
                    }
                    break;
                case JTokenType.Float:
                    {
                        result = (double)value;
                    }
                    break;
                case JTokenType.Raw:
                case JTokenType.Bytes:
                case JTokenType.Date:
                case JTokenType.Guid:
                case JTokenType.TimeSpan:
                case JTokenType.Uri:
                case JTokenType.String:
                    {
                        result = (string)value;
                    }
                    break;
                case JTokenType.Undefined:
                case JTokenType.Null:
                    {
                        result = null;
                    }
                    break;

            }

            return result;

        }
        #endregion
        public Dictionary<string, dynamic> properties
        {
            get
            {
                return _members;
            }
        }
        public override bool TryGetMember(GetMemberBinder binder, out dynamic result)
        {
            result = get_value(binder.Name);
            return true;
        }
        private dynamic get_value(string name)
        {
            dynamic result;
            if (_members.ContainsKey(name))
            {
                result = _members[name];
            }
            else
            {
                result = new flex();
                _members.Add(name, result);
            }
            return result;
        }
        private void set_value(string name, dynamic value)
        {
            if (_members.ContainsKey(name))
            {
                _members[name] = value;
            }
            else
            {
                _members.Add(name, value);
            }
        }
        public T get<T>(string key)
        {
            var value = _members[key];
            if (value == null)
            {
                return value;
            }
            Type comparer = typeof(T);
            if (comparer == typeof(string))
            {
                return value.ToString();
            }
            var nullable = Nullable.GetUnderlyingType(typeof(T));
            if (nullable != null)
            {
                comparer = nullable;
            }
            if (value != null)
            {
                if (value.GetType() != comparer)
                {
                    return (T)Convert.ChangeType(value, comparer);
                }
                else
                {
                    return (T)value;
                }
            }
            return value;
        }
        public List<T> get_list<T>(string key)
        {
            var value = _members[key];
            if (value == null)
            {
                return null;
            }
            var res = new List<T>();
            var items = (List<object>)value;
            foreach (var item in items)
            {
                res.Add((T)item);
            }
            return res;
        }

        public void set<T>(string key, T value)
        {
            if (_members.ContainsKey(key))
            {
                _members[key] = value;
            }
            else
            {
                _members.Add(key, value);
            }
        }

        public override bool TrySetMember(SetMemberBinder binder, dynamic value)
        {
            set_value(binder.Name, value);
            return true;
        }
        public dynamic this[string name]
        {
            get
            {
                return get_value(name);
            }
            set
            {
                set_value(name, value);
            }
        }
        public bool contains(string key)
        {
            return _members.ContainsKey(key);
        }
        public void remove(string key)
        {
            _members.Remove(key);
        }

        #region to json
        private string object_to_json(flex obj)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append('{');
            var en = obj._members.GetEnumerator();
            if (en.MoveNext())
            {
                sb.AppendFormat("\"{0}\":{1}", en.Current.Key, member_to_json_key(en.Current.Value));
                while (en.MoveNext())
                {
                    sb.AppendFormat(", \"{0}\":{1}", en.Current.Key, member_to_json_key(en.Current.Value));
                }
            }
            sb.Append('}');
            return sb.ToString();
        }
        private string member_to_json_key(dynamic member)
        {
            var sb = new StringBuilder();
            if (member == null)
            {
                return "null";
            }
            else if (member.GetType() == typeof(string))
            {
                var v = member.Replace("\\", "\\\\");
                v = v.Replace("\"", "\\\"");
                v = v.Replace("\r", "");
                v = v.Replace("\n", "\\n");
                v = v.Replace("\t", "\\t");
                sb.AppendFormat("\"{0}\"", v);
            }
            else if (member.GetType() == typeof(bool))
            {
                if (member)
                {
                    sb.Append("true");
                }
                else
                {
                    sb.Append("false");
                }
            }
            else if (member.GetType() == typeof(int) || member.GetType() == typeof(long))
            {
                sb.Append(member);
            }
            else if (member.GetType() == typeof(double) || member.GetType() == typeof(float) || member.GetType() == typeof(decimal))
            {
                sb.Append(member.ToString(CultureInfo.InvariantCulture));
            }
            else if (member.GetType() == typeof(flex))
            {
                return object_to_json(member);
            }
            else if (member.GetType().IsArray)
            {
                sb.Append('[');

                if (member.Length > 0)
                {
                    sb.Append(member_to_json_key(member[0]));
                    for (int i = 1; i < member.Length; i++)
                    {
                        sb.AppendFormat(", {0}", member_to_json_key(member[i]));
                    }
                }
                sb.Append(']');
            }
            else if (member is IEnumerable)
            {
                sb.Append('[');
                var en = member.GetEnumerator();
                if (en.MoveNext())
                {
                    sb.Append(member_to_json_key(en.Current));
                    while (en.MoveNext())
                    {
                        sb.AppendFormat(", {0}", member_to_json_key(en.Current));
                    }
                }
                sb.Append(']');
            }
            else
            {
                sb.AppendFormat("\"{0}\"", member);
            }

            return sb.ToString();
        }
        public string json()
        {
            return object_to_json(this);
        }
        #endregion
    }
}
