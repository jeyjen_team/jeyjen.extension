﻿using System;

namespace jeyjen.extension
{
    public static class object_extension
    {
        public static bool is_null(this object o)
        {
            return o == null;
        }

        public static T value_is_null<T>(this object o, T value)
        {
            dynamic obj = o;
            if (o == null)
            {
                return value;
            }
            else
            {
                Type comparer = typeof(T);
                if (comparer == typeof(string))
                {
                    return obj.ToString();
                }
                var nullable = Nullable.GetUnderlyingType(typeof(T));
                if (nullable != null)
                {
                    comparer = nullable;
                }
                if (o.GetType() != comparer)
                {
                    return (T)Convert.ChangeType(o, comparer);
                }
                else
                {
                    return obj;
                }
            }
        }

        public static bool is_equals(this object o, object compared)
        {
            if (o == null)
            {
                if (compared == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return o.Equals(compared);
        }
    }
}
