﻿using Antlr3.ST;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace jeyjen.extension
{
    public static class string_extension
    {
        public static string format(this string s, params object[] plhs)
        {
            return string.Format(s, plhs);
        }

        public static bool is_empty(this string s)
        {
            return s == "";
        }

        public static bool is_null_or_empty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }

        public static bool is_null_or_whitespace(this string s)
        {
            return string.IsNullOrWhiteSpace(s);
        }

        public static flex json(this string text)
        {
            return new flex(text);
        }
        public static XDocument xml(this string text)
        {
            return XDocument.Parse(text.Trim());
        }

        public static template param(this string text, string key, object value)
        {
            var t = new template(text);
            t.param(key, value);
            return t;
        }
    }

    public class template
    {
        private StringTemplate _template;
        public template(string text)
        {
            _template = new StringTemplate(text);
        }

        public template param(string key, object value)
        {
            _template.SetAttribute(key, value);
            return this;
        }

        public override string ToString()
        {
            return _template.ToString();
        }

        public static implicit operator string(template d)
        {
            return d.ToString();
        }
    }
}
