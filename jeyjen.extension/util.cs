﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jeyjen.extension
{
    internal class util
    {
        public static T convert<T>(object value)
        {
            dynamic v = null;
            Type comparer = typeof(T);
            var nullable = Nullable.GetUnderlyingType(typeof(T));
            if (nullable != null)
            {
                comparer = nullable;
            }
            if (value != null)
            {
                if (value.GetType() != comparer)
                {
                    return (T)Convert.ChangeType(value, comparer);
                }
                else
                {
                    return (T)value;
                }
            }
            return v;
        }
    }
}
