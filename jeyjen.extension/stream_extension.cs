﻿using System;
using System.IO;

namespace jeyjen.extension
{
    public static class stream_extension
    {
        public static byte[] read(this Stream stream, int length)
        {
            byte[] buffer = new byte[length];
            if (length == 0)
            {
                return buffer;
            }

            int offset = 0;
            do
            {
                int read = stream.Read(buffer, offset, length - offset);
                if (read == 0)
                {
                    throw new EndOfStreamException(string.Format("Unexpected end of stream encountered whilst attempting to read {0:#,##0} bytes", length));
                }

                offset += read;
            } while (offset < length);

            return buffer;
        }

        public static ushort read_ushort(this Stream stream, bool is_little_endian = true)
        {
            byte[] buffer = stream.read(2);

            if (! is_little_endian)
            {
                Array.Reverse(buffer); // big endian
            }
            
            return BitConverter.ToUInt16(buffer, 0);
        }

        public static ulong read_ulong(this Stream stream, bool is_little_endian = true)
        {
            byte[] buffer = stream.read(8);

            if (! is_little_endian)
            {
                Array.Reverse(buffer); // big endian
            }

            return BitConverter.ToUInt64(buffer, 0);
        }

        public static uint read_uint(this Stream stream, bool is_little_endian = true)
        {
            byte[] buffer = stream.read(4);

            if (! is_little_endian)
            {
                Array.Reverse(buffer); // big endian
            }

            return BitConverter.ToUInt32(buffer, 0);
        }


        public static long read_long(this Stream stream, bool is_little_endian = true)
        {
            byte[] lenBuffer = stream.read(8);

            if (! is_little_endian)
            {
                Array.Reverse(lenBuffer); // big endian
            }

            return BitConverter.ToInt64(lenBuffer, 0);
        }

        public static void write_ulong(this Stream stream, ulong value, bool is_little_endian = true)
        {
            byte[] buffer = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian && ! is_little_endian)
            {
                Array.Reverse(buffer);
            }

            stream.Write(buffer, 0, buffer.Length);
        }

        public static void write_long(this Stream stream, long value, bool is_little_endian = true)
        {
            byte[] buffer = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian && ! is_little_endian)
            {
                Array.Reverse(buffer);
            }

            stream.Write(buffer, 0, buffer.Length);
        }

        public static void write_ushort(this Stream stream, ushort value, bool is_little_endian = true)
        {
            byte[] buffer = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian && ! is_little_endian)
            {
                Array.Reverse(buffer);
            }

            stream.Write(buffer, 0, buffer.Length);
        }


    }
}
