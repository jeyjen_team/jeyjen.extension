﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jeyjen.extension
{
    public class promise<result_type>
    {
        private result_type _result;
        private Exception _exception;
        private state _state;
        private Action<result_type> _then;
        private Action<Exception> _catch;
        private enum state
        {
            none,
            result_set,
            exception_set
        }

        public promise()
        {
            _state = state.none;
        }

        public void result(result_type result)
        {
            if (_state == state.none)
            {
                _state = state.result_set;
                _result = result;
            }
            else
            {
                if (_state == state.result_set)
                {
                    throw new Exception("result is was set yet");
                }
                else
                {
                    throw new Exception("error is was set yet");
                }
            }
        }

        public void error(Exception exception)
        {
            if (_state == state.none)
            {
                _state = state.exception_set;
                _exception = exception;
            }
            else
            {
                if (_state == state.result_set)
                {
                    throw new Exception("result is was set yet");
                }
                else
                {
                    throw new Exception("error is was set yet");
                }
            }
        }

        public promise<result_type> on_result(Action<result_type> on_result)
        {
            _then = on_result;
            return this;
        }

        public promise<result_type> on_catch(Action<Exception> on_catch)
        {
            _catch = on_catch;
            return this;
        }

        public void resolve()
        {
            if (_state == state.result_set && _then != null)
            {
                _then(_result);
            }
            else if (_state == state.exception_set && _catch != null)
            {
                _catch(_exception);
            }
        }
    }
}
