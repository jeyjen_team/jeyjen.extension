﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using jeyjen.extension;
using System.Collections.Generic;

namespace test
{
    [TestClass]
    public class string_ext
    {
        [TestMethod]
        public void is_null()
        {
            string s = null;
            s.is_null();
        }

        [TestMethod]
        public void is_equals()
        {
            string s = "asd2";
            var r = s.is_equals("asd");
        }

        [TestMethod]
        public void is_empty()
        {
            string s = null;
            var res = s.is_empty();
            var res2 = "";
        }

        [TestMethod]
        public void join_to_string_array()
        {
            int[] ns = new int[] {1, 6, 78};
            var res = ns.join_to_string(",");
        }

        [TestMethod]
        public void join_to_string_list()
        {
            var list = new List<int>() { 1, 3, 6};
            var res = list.join_to_string(",");
        }

        [TestMethod]
        public void join_to_string_set()
        {
            var set = new HashSet<int>() { 1, 3, 6 };
            var res = set.join_to_string(",", true);
        }
    }
}
