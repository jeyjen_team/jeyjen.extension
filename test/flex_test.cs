﻿using jeyjen.extension;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    [TestClass]
    public class flex_test
    {
        [TestMethod]
        public void get_items()
        {
            flex b = new flex("{\"arr\":[{\"p1\":1}, {\"p1\":2}]}");

            var items = b.get_list<flex>("arr");
            foreach (var i in items)
            {
                Debug.WriteLine(i.get<string>("p1"));
            }
        }


        [TestMethod]
        public void newtonjson()
        {
            flex b = new flex(JObject.Parse("{\"arr\":[{\"p1\":1}, {\"p1\":2}]}"));

            var items = b.get_list<flex>("arr");
            foreach (var i in items)
            {
                Debug.WriteLine(i.get<string>("p1"));
            }
        }
    }
}
