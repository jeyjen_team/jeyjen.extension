﻿using jeyjen.extension;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    [TestClass]
    public class promise
    {
        [TestMethod]
        public void promise_success()
        {
            success()
                .on_result((r) =>
                {
                    Debug.WriteLine("result {0}", r);
                })
                .on_catch((e) =>
                {
                    Debug.WriteLine("error {0}", e.Message);
                })
                .resolve();
        }
        [TestMethod]
        public void promise_error()
        {
            fail()
                .on_result((r) =>
                {
                    Debug.WriteLine("result {0}", r);
                })
                .on_catch((e) =>
                {
                    Debug.WriteLine("error {0}", e.Message + "123");
                })
                .resolve();
        }

        public promise<int> success()
        {
            var r = new promise<int>();
            try
            {
                r.result(22);
                return r;
            }
            catch (Exception e)
            {
                r.error(e);
            }
            return r;
        }

        public promise<int> fail()
        {
            var r = new promise<int>();
            try
            {
                throw new Exception("error message");
                r.result(22);
                return r;
            }
            catch (Exception e)
            {
                r.error(e);
            }
            return r;
        }
    }
}
